import Link from "next/link";

export default function Hero({ heroImage, heroTagline }) {
  return (
    <div
      className="hero min-h-[50vh] md:min-h-screen"
      style={{
        backgroundImage: `url(${heroImage.url})`,
      }}
    >
      <div className="hero-overlay bg-opacity-60"></div>
      <div className="hero-content text-center text-base-100">
        <div className="max-w-3xl">
          <h1 className="mb-10 text-3xl md:text-5xl font-normal font-oswald">
            Gift of Education
          </h1>
          <p className="mb-10 text-lg md:text-xl font-openSans">
            {heroTagline}
          </p>
          <Link className="btn btn-primary" href="/about">
            Learn more about us
          </Link>
        </div>
      </div>
    </div>
  );
}

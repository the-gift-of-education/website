import RenderMember from "./renderMember";

export default function RenderMemberSection({ activeDepartment, members }) {
  // Filter members by active Department
  function filterDepartments(activeDepartment) {
    switch (activeDepartment) {
      // Team and Advisory board use same filter method
      case "Team":
      case "Advisory Board":
        return members.filter((item) => item.orgSection === activeDepartment);
      case "Partners & Sponsors":
        return members.filter((item) =>
          ["Partners", "Sponsors", "Patrons"].includes(item.orgSection),
        );
      // Filter Instructors/Assistants and them sort them into specified order.
      case "Instructors & Assistants":
        const filteredItems = members.filter((item) =>
          [
            "Instructors",
            "Teaching Assistants",
            "Administrative Assistants",
          ].includes(item.orgSection),
        );
        return filteredItems.sort((a, b) => {
          const order = [
            "Instructors",
            "Teaching Assistants",
            "Administrative Assistants",
          ];
          return order.indexOf(a.orgSection) - order.indexOf(b.orgSection);
        });
      default:
        return [];
    }
  }

  const filteredDepartments = filterDepartments(activeDepartment);

  return (
    <div className="flex flex-col md:flex-row md:flex-wrap items-center justify-center mx-auto w-[75%] gap-9">
      {filteredDepartments.map(
        ({ orgSection, membersCollection: { items } }) => {
          return (
            <div key={orgSection} className="flex flex-col items-center ">
              <h1 className="text-2xl font-oswald mb-16">{orgSection}</h1>
              <div className="flex flex-col md:flex-row md:flex-wrap items-center justify-center gap-9">
                {items.map((member) => (
                  <RenderMember
                    {...{ activeDepartment, member }}
                    key={member.sys.id}
                  />
                ))}
              </div>
            </div>
          );
        },
      )}
    </div>
  );
}

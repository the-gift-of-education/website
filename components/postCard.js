import formatDate from "@/lib/utils/formatDate";
import Link from "next/link";

export default function PostCard({ post, postPath, displayTags }) {
  return (
    <div className="card rounded-lg bg-[#F7F7F7] border border-goeGrey/10 text-primary-content md:w-[700px] mb-8">
      <div className="card-body text-left">
        <Link href={postPath}>
          <h2 className="card-title font-openSans">{post.title}</h2>
        </Link>
        <p className="font-openSans mb-4 text-sm">
          {formatDate(post.dateTime)}
        </p>
        <p className="font-openSans">{post.description}</p>
        <div className="card-actions mt-4">
          {displayTags && (
            <div className="flex self-end mr-8">
              {post.tags.map((tag) => {
                return (
                  <div
                    className="font-openSans cursor-pointer hover:bg-primary/80 bg-primary inline-flex items-center mr-2 mt-2 px-3 py-1 rounded-full text-xs font-medium text-goeGrey"
                    key={tag}
                  >
                    {tag}
                  </div>
                );
              })}
            </div>
          )}
          <Link
            className="btn btn-primary hover:bg-primary/90 ml-auto self-end font-openSans"
            href={postPath}
          >
            Read More
          </Link>
        </div>
      </div>
    </div>
  );
}

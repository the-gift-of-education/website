const Statement = ({ heading, textField, bgColor }) => {
  return (
    <div
      className={`flex flex-col justify-center md:justify-start md:w-1/2 text-base-100 ${bgColor} p-[50px] h-[300px] md:h-auto`}
    >
      <h1 className="font-oswald text-xl md:text-2xl mb-4">{heading}</h1>
      <p className="font-openSans">{textField}</p>
    </div>
  );
};

export default function MissionVision({ missionStatement, visionStatement }) {
  return (
    <div className="flex flex-col w-full lg:flex-row">
      <Statement
        heading="Our Misison"
        textField={missionStatement}
        bgColor="bg-[#222222]"
      />
      <Statement
        heading="Our Vision"
        textField={visionStatement}
        bgColor="bg-secondary"
      />
    </div>
  );
}

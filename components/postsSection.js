"use client";

import { useState } from "react";
import RenderPosts from "./renderPosts";
import PostsFilter from "./postsFilter";

export default function PostsSection({ posts }) {
  // Set the default post view to the current year.
  const currentYear = new Date().getFullYear().toString();
  const [selectedYear, setSelectedYear] = useState(currentYear);
  // Filter Posts by selected year.
  const selectedYearPosts = posts.find((post) => post.year === selectedYear);

  const handleYearClick = (year) => {
    setSelectedYear(year);
    setSelectedTag("all tags");
  };

  // Set the default tag filter to "all tags".
  const [selectedTag, setSelectedTag] = useState("all tags");

  const handleSelectTag = (tag) => {
    selectedTag === tag ? setSelectedTag("all tags") : setSelectedTag(tag);
  };

  return (
    <div className="flex flex-col md:flex-row p-8 w-[80%] mx-auto">
      <PostsFilter
        {...{
          handleSelectTag,
          handleYearClick,
          posts,
          selectedTag,
          selectedYear,
          selectedYearPosts,
        }}
      />
      <RenderPosts
        {...{ selectedTag, selectedYear }}
        posts={selectedYearPosts.posts}
      />
    </div>
  );
}

export default function MembersNav({
  activeDepartment,
  handleSetActiveDepartment,
}) {
  const nav = [
    "Team",
    "Advisory Board",
    "Partners & Sponsors",
    "Instructors & Assistants",
  ];

  return (
    <div className="flex flex-row items-center w-[80%] justify-evenly mx-auto py-16">
      {nav.map((item) => {
        return (
          <h1
            key={item}
            className={`font-oswald text-xl cursor-pointer ${activeDepartment === item ? "underline" : "hover:underline"}`}
            onClick={() => handleSetActiveDepartment(item)}
          >
            {item}
          </h1>
        );
      })}
    </div>
  );
}

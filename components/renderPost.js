import formatDate from "@/lib/utils/formatDate";
import ReactMarkdown from "react-markdown";

const RenderPostDetails = ({ author, banner, dateTime, tags }) => {
  return (
    <>
      <div className="flex flex-row items-center justify-center mb-4">
        <p className={`mr-8 ${banner && "text-lg"}`}>By: {author.name}</p>
        <p className={`${banner && "text-lg"}`}>{formatDate(dateTime)}</p>
      </div>
      {tags && (
        <ul className="flex items-center justify-center mb-6">
          {tags.map((tag) => (
            <li
              key={tag}
              className={`badge badge-primary mr-4 ${banner && "text-lg px-4 py-3"}`}
            >
              {tag}
            </li>
          ))}
        </ul>
      )}
    </>
  );
};

export default function RenderPost({
  blogPost: { author, body, dateTime, postBannerImage, tags, title },
}) {
  return (
    <>
      {postBannerImage ? (
        <div
          className="hero h-[450px] md:h-[650px]"
          style={{
            backgroundImage: `url(${postBannerImage.url})`,
          }}
        >
          <div className="hero-overlay bg-opacity-50"></div>
          <div className="hero-content text-center text-base-100">
            <div className="max-w-3xl">
              <h1 className="mb-10 text-3xl md:text-5xl font-normal font-oswald">
                {title}
              </h1>
              <RenderPostDetails
                {...{ author, dateTime, tags }}
                banner={true}
              />
            </div>
          </div>
        </div>
      ) : (
        <div className="w-[80%] mx-auto text-center pt-16 pb-6">
          <h1 className="font-oswald text-4xl font-normal">{title}</h1>
        </div>
      )}
      <div className="w-[80%] mx-auto font-openSans min-h-[65vh]">
        <div className={`${postBannerImage ? "hidden" : ""}`}>
          <RenderPostDetails {...{ author, dateTime, tags }} />
        </div>
        <ReactMarkdown className="prose md:prose-lg max-w-full p-8 md:px-24">
          {body}
        </ReactMarkdown>
      </div>
    </>
  );
}

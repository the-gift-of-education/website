import Link from "next/link";
import Logo from "./logo";

export const routes = [
  {
    label: "Home",
    path: "/",
  },
  {
    label: "About",
    path: "/about",
  },
  {
    label: "Members",
    path: "/members",
  },
  {
    label: "Projects",
    path: "/projects",
  },
  {
    label: "Blog",
    path: "/blog",
  },
  {
    label: "Donate",
    path: "/donate",
  },
];

const renderRoutes = (addStyle) => {
  return routes.map((route) => (
    <li
      key={route.label}
      className={`text-base-100 font-oswald text-base ${addStyle ? addStyle : ""} text-base-100 active:text-base-100 visited:text-base-100`}
    >
      <Link href={route.path}>{route.label}</Link>
    </li>
  ));
};

export default function Nav() {
  return (
    <div className="navbar bg-neutral px-8">
      {/* Logo */}
      <div className="navbar-start">
        <Link className="flex items-center" href="/">
          <Logo />
          <p className="text-base-100 font-oswald text-base font-extralight tracking-widest leading-6 ml-4 no-underline p-0">
            Gift of Education
          </p>
        </Link>
      </div>
      {/* Desktop Menu */}
      <ul className="flex-row hidden md:flex w-1/2 justify-between">
        {renderRoutes()}
      </ul>
      <div className="navbar-end">
        {/* Mobile Dropdown Menu */}
        <div className="dropdown dropdown-end">
          <div tabIndex={0} role="button" className="btn btn-neutral lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className="menu menu-lg dropdown-content z-[1] p-2 shadow bg-neutral rounded-box w-52"
          >
            {renderRoutes()}
          </ul>
        </div>
        {/* Alert */}
        <button className=" hidden md:block btn btn-ghost mr-5">
          <div className="indicator">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="#FFFFFF"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
              />
            </svg>
            <span className="badge badge-xs badge-primary indicator-item"></span>
          </div>
        </button>
        {/* Donate Button */}
        <Link className="btn btn-primary hidden md:flex" href="/donate">
          Donate Now
        </Link>
      </div>
    </div>
  );
}

export default function TagsByYear({ tags, selectedTag, handleSelectTag }) {
  return (
    <>
      <h3 className="mb-2 font-openSans font-semibold">Filter by tag:</h3>
      <div className="flex flex-wrap w-[275px]">
        {tags.map((tag) => (
          <p
            className={`badge px-4 py-3 mr-2 mb-2 cursor-pointer ${selectedTag === tag ? "bg-primary hover:bg-primary/70 hover:border-primary/20" : selectedTag === "all tags" ? "bg-primary hover:bg-primary/40 hover:border-primary/10" : "bg-primary/40 hover:bg-primary hover:border-primary"}`}
            key={tag}
            onClick={() => handleSelectTag(tag)}
          >
            {tag}
          </p>
        ))}
      </div>
    </>
  );
}

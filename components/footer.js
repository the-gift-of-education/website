import Link from "next/link";
import { routes } from "./nav";

export default function Footer() {
  return (
    <footer className="footer p-10 bg-neutral text-neutral-content flex flex-col justify-center items-center">
      <ul className="flex flex-col md:flex-row items-center justify-center w-2/3 mx-auto">
        {routes.map((route) => (
          <li
            className="text-base-100 font-oswald text-lg md:mr-8"
            key={route.label}
          >
            <Link href={route.path}>{route.label}</Link>
          </li>
        ))}
      </ul>
      <a href="https://www.contentful.com" rel="noreferrer" target="_blank">
        <img
          src="https://images.ctfassets.net/fo9twyrwpveg/7F5pMEOhJ6Y2WukCa2cYws/398e290725ef2d3b3f0f5a73ae8401d6/PoweredByContentful_DarkBackground.svg"
          alt="Powered by Contentful"
          className="w-[125px] md:w-[150px]"
        />
      </a>
    </footer>
  );
}

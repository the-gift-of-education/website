import PostCard from "@/components/postCard";
import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { projectPostsQuery } from "@/lib/queries/projectsQuery";

export default async function Projects() {
  const query = projectPostsQuery;

  const {
    blogPostCollection: { items },
  } = await makeGraphQLRequest({ query });

  return (
    <div>
      <div className="w-[80%] mx-auto text-center py-12">
        <h1 className="font-oswald text-4xl md:text-5xl font-normal">
          Projects
        </h1>
      </div>
      <div className="flex flex-col p-8 w-[80%] mx-auto items-center">
        {items.map((post) => {
          const postPath = `/projects/${post.slug}?id=${post.sys.id}`;
          return (
            <PostCard
              {...{ post, postPath }}
              key={post.sys.id}
              displayTags={false}
            />
          );
        })}
      </div>
    </div>
  );
}

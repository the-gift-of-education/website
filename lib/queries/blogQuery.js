export const blogPostQuery = `
  query getBlogPost($id: String!){
    blogPost(id: $id) {
      postBannerImage {
        url
      }
      title
      dateTime
      author {
        name
      }
      tags
      body
    }
  }
`;

export const blogPostsQuery = `
{
	blogPostCollection(where: { postType: "Blog" }){
    items {
      sys {
        id
        firstPublishedAt
      }
      title
      description
      dateTime
      slug
      tags
    }
  }
}
`;

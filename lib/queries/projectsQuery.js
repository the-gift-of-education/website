export const projectPostQuery = `
  query getBlogPost($id: String!){
    blogPost(id: $id) {
      postBannerImage {
        url
      }
      slug
      title
      dateTime
      author {
        name
      }
      body
    }
  }
`;

export const projectPostsQuery = `
{
	blogPostCollection(where: { postType: "Project" }){
    items {
      sys {
        id
        firstPublishedAt
      }
      title
      description
      dateTime
      slug
      tags
    }
  }
}
`;
